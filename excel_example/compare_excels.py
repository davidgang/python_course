import sys
from openpyxl import load_workbook
from itertools import islice

def read_file(file1):
    wb = load_workbook(filename = file1)
    ws = wb['SAMPLE']
    prices = {}
    for value in islice(ws.values,2,None):
        prices[(value[0], value[3], value[6])] = value
    return prices
        
def compare_hashes(dict1, dict2):
    just_in_dict1 = dict1.keys() - dict2.keys()
    just_in_dict2 = dict2.keys() - dict1.keys()
    in_both = dict1.keys() & dict2.keys()
    print("just in file2")
    for key in just_in_dict2:
        print(dict2[key])    
    print("just in file1")
    for key in just_in_dict1:
        print(dict1[key])

    print("In both but different")
    for key in in_both:
        if dict1[key] != dict2[key]:
            print("file1",dict1[key])
            print("file2",dict2[key])

def compare_files(file1, file2):
    dict1 = read_file(file1)
    dict2 = read_file(file2)
    compare_hashes(dict1, dict2)

if __name__ == '__main__':
    compare_files(sys.argv[1], sys.argv[2])